﻿using CustomizeControl.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;

namespace CustomizeControl.View
{
    public class ExitWindowAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            var window = Window.GetWindow(AssociatedObject);
            window.Close();
        }
    }

    public class SetResultAndCloseAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            var args = parameter as MessageEventArgs<bool>;
            if (args != null)
            {
                var result = (bool)args.Message.Body;
                var window = Window.GetWindow(AssociatedObject);
                window.DialogResult = result;
            }
        }
    }

    public class NoticeAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            var args = parameter as MessageEventArgs<MessageArgs, bool>;
            if (args != null)
            {
                var parent = Window.GetWindow(AssociatedObject);
                var dialog = new MessageView(args.Message.Body);
                dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                dialog.Owner = parent;
                var result = (bool)dialog.ShowDialog();
                if (args.Callback != null)
                {
                    args.Message.Response = result;
                    args.Callback(args.Message);
                }
            }

            //var result = MessageBox.Show(
            //    args.Message.Body.ToString(),
            //    "確認",
            //    MessageBoxButton.OKCancel);

            //args.Message.Response = result == MessageBoxResult.OK;
            //args.Callback(args.Message);
        }
    }

}
