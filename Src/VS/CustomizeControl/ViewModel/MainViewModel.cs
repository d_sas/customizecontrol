﻿using CustomizeControl.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomizeControl.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        ////////////////////////////////////////
        //Button
        ////////////////////////////////////////
        public DelegateCommand<string> PushButtonCommand { get { return this.pushButtonCommand ?? (this.pushButtonCommand = new DelegateCommand<string>(PushButton)); } }
        private DelegateCommand<string> pushButtonCommand;
        private void PushButton(string button)
        {
            switch (button)
            {
            case "1": DefaultButtonCount++; break;
            case "2": CustomButtonCount++; break;
            }
            //AlertMessenger.Raise(
            //    new Message<MessageArgs, bool>(new MessageArgs(MessageType.Ok, IconType.Warning, "確認", "押しました.")),
            //    null
            //    );
        }
        public int DefaultButtonCount { get { return this.defaultButtonCount; } set { this.defaultButtonCount = value; RaisePropertyChanged(nameof(DefaultButtonCount)); } }
        private int defaultButtonCount;
        public int CustomButtonCount { get { return this.customButtonCount; } set { this.customButtonCount = value; RaisePropertyChanged(nameof(CustomButtonCount)); } }
        private int customButtonCount;

    }
}
