﻿using CustomizeControl.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomizeControl.Utility
{
    public class ViewModelBase : PropertyChangeNotifier
    {
        public Messenger ExitWindowMessenger { get { return this.exitWindowMessenger ?? (this.exitWindowMessenger = new Messenger()); } }
        private Messenger exitWindowMessenger;

        public Messenger<bool> SetWindwoResultAndExitMessenger { get { return this.setWindwoResultAndExitMessenger ?? (this.setWindwoResultAndExitMessenger = new Messenger<bool>()); } }
        private Messenger<bool> setWindwoResultAndExitMessenger;

        public Messenger<MessageArgs, bool> AlertMessenger { get { return this.alertMessenger ?? (this.alertMessenger = new Messenger<MessageArgs, bool>()); } }
        private Messenger<MessageArgs, bool> alertMessenger;
    }
}
