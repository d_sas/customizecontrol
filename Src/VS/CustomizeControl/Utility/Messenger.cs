﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interactivity;

namespace CustomizeControl.Utility
{
    public class Message<TParam, TResult>
    {
        public TParam Body { get; private set; }
        public TResult Response { get; set; }

        public Message(TParam body)
        {
            Body = body;
        }
    }

    public class Message<TParam>
    {
        public TParam Body { get; private set; }

        public Message(TParam body)
        {
            Body = body;
        }
    }

    public class MessageEventArgs<TParam, TResult> : EventArgs
    {
        public Message<TParam, TResult> Message { get; private set; }
        public Action<Message<TParam, TResult>> Callback { get; private set; }

        public MessageEventArgs(Message<TParam, TResult> message, Action<Message<TParam, TResult>> callback)
        {
            Message = message;
            Callback = callback;
        }
    }

    public class MessageEventArgs<TParam> : EventArgs
    {
        public Message<TParam> Message { get; private set; }
        public Action<Message<TParam>> Callback { get; private set; }

        public MessageEventArgs(Message<TParam> message, Action<Message<TParam>> callback)
        {
            Message = message;
            Callback = callback;
        }
    }

    public class Messenger<TParam, TResult>
    {
        public event EventHandler<MessageEventArgs<TParam, TResult>> Raised;

        public void Raise(Message<TParam, TResult> message, Action<Message<TParam, TResult>> callback)
        {
            var raised = this.Raised;
            if (raised != null)
            {
                raised(this, new MessageEventArgs<TParam, TResult>(message, callback));
            }
        }
    }

    public class Messenger<TParam>
    {
        public event EventHandler<MessageEventArgs<TParam>> Raised;

        public void Raise(Message<TParam> message, Action<Message<TParam>> callback)
        {
            var raised = this.Raised;
            if (raised != null)
            {
                raised(this, new MessageEventArgs<TParam>(message, callback));
            }
        }
    }

    public class Messenger
    {
        public event EventHandler Raised;

        public void Raise()
        {
            var raised = this.Raised;
            if (raised != null)
            {
                raised(null, null);
            }
        }
    }

    public class MessageTrigger : EventTrigger
    {
        protected override string GetEventName()
        {
            return "Raised";
        }
    }
}
